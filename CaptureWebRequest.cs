﻿//-----------------------------------------------------------------------
// <copyright file="CaptureWebRequest.cs" company="Viacom">
//    All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Viacom.Adspace.AnalyticsHttpModule
{
    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Administration;
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Security.Principal;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Xml;
    using Viacom.AdSpace.Shared;

    /// <summary>
    /// Class to capture web request
    /// </summary>
    public class CaptureWebRequest : IHttpModule, IDisposable
    {
        private HttpContext context = null;
        string userName = string.Empty;
        SqlConnection sqlConnection;

        /// <summary>
        /// Constructor
        /// </summary>
        public CaptureWebRequest() { }

        /// <summary>
        /// Dispose objects
        /// </summary>
        /// <param name="disposing"> true false value</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (sqlConnection != null)
                    sqlConnection.Close();
            }
        }

        /// <summary>
        /// Dispose Objects
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Initialize call
        /// </summary>
        /// <param name="context">Context of HttpApplication class</param>
        public void Init(HttpApplication context)
        {
            try
            {
                context.AuthenticateRequest += Context_AuthenticateRequest;
                context.PostRequestHandlerExecute += Context_PostRequestHandlerExecute;
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Analytics -- Init", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
        }

        /// <summary>
        /// Execute Authenticate request to capture request
        /// </summary>
        /// <param name="sender">get HttpApplication object</param>
        /// <param name="e">get event args</param>
        void Context_AuthenticateRequest(object sender, EventArgs e)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                WindowsImpersonationContext impersonatedContext = null;
                try
                {
                    context = ((HttpApplication)sender).Context;
                    if (context.Request.Url.ToString().ToLower().Contains(HttpModuleConstants.CellStorageService))
                    {
                        System.IO.Stream str; String strmContents;
                        Int32 strLen, strRead;
                        str = context.Request.InputStream;
                        strLen = Convert.ToInt32(str.Length);
                        byte[] strArr = new byte[strLen];
                        strRead = str.Read(strArr, 0, strLen);
                        str.Position = 0;
                        strmContents = System.Text.Encoding.Default.GetString(strArr);
                        if (!string.IsNullOrEmpty(strmContents))
                        {
                            if (strmContents.Contains(HttpModuleConstants.EnvelopeSTag))
                            {
                                if (context.User != null)
                                {
                                    string userName = context.User.Identity.Name;
                                    bool isValidUser = IsValidUser(userName);
                                    if (isValidUser)
                                    {
                                        string[] str1 = Regex.Split(strmContents, HttpModuleConstants.EnvelopeSTag);
                                        string[] str2 = Regex.Split(str1[1], HttpModuleConstants.EnvelopeETag);
                                        string validValue = HttpModuleConstants.EnvelopeSTag + str2[0] + HttpModuleConstants.EnvelopeETag;
                                        using (XmlReader reader = XmlReader.Create(new StringReader(validValue)))
                                        {
                                            while (reader.Read())
                                            {
                                                reader.ReadToFollowing("SubRequest");
                                                string type = reader.GetAttribute("Type");
                                                if (type == "Cell")
                                                {
                                                    XmlReader reader1 = XmlReader.Create(new StringReader(validValue));
                                                    reader1.ReadToFollowing(HttpModuleConstants.RequestXMLNode);
                                                    string url = HttpContext.Current.Server.UrlDecode(reader1.GetAttribute(HttpModuleConstants.UrlXMLAttribute));
                                                    WindowsIdentity spUserIdentity = WindowsIdentity.GetCurrent();
                                                    impersonatedContext = spUserIdentity.Impersonate();
                                                    //save request in DB
                                                    SaveRequestInDB(context.User.Identity.Name, url, url, GetUserIP(context), HttpModuleConstants.Browser);
                                                    break;
                                                }
                                            }                                             
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Analytics -- context_AuthenticateRequest", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                }
                finally
                {
                    if (impersonatedContext != null) { impersonatedContext.Undo(); }
                }
            });
        }

        /// <summary>
        /// Execute Post request
        /// </summary>
        /// <param name="sender">get HttpApplication object</param>
        /// <param name="e">get event args</param>
        private void Context_PostRequestHandlerExecute(object sender, EventArgs e)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                WindowsImpersonationContext impersonatedContext = null;
                try
                {
                    context = ((HttpApplication)sender).Context;
                    var Request = context.Request;
                    //When request will not come from browser so skip it like Office Client Application, etc. track these request from AuthenticationRequest method.
                    if (!string.Equals(Request.Browser.Browser, HttpModuleConstants.UnknownBrowser))
                    {
                        string siteCollectionUrl = string.Empty;
                        siteCollectionUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                        if (Request.Url.Segments.Length > 3)
                        {
                            if (Request.Url.Segments[1] != "sites/")
                            {
                                var uri = new UriBuilder(HttpContext.Current.Request.Url);
                                uri.Path = HttpContext.Current.Request.RawUrl;
                                siteCollectionUrl = HttpContext.Current.Server.UrlDecode(uri.Uri.AbsoluteUri.ToLower()).Split(new[] { '?' })[0];
                                siteCollectionUrl = siteCollectionUrl.Replace(HttpContext.Current.Request.CurrentExecutionFilePath.ToLower(), "");
                            }
                            else
                            {
                                siteCollectionUrl = siteCollectionUrl + Request.Url.Segments[0] + Request.Url.Segments[1] + Request.Url.Segments[2].TrimEnd('/');
                            }
                        }
                        bool isValidSiteCollection = ValidSiteCollection(siteCollectionUrl);
                        if (isValidSiteCollection)
                        {
                            WindowsIdentity spUserIdentity = WindowsIdentity.GetCurrent();
                            impersonatedContext = spUserIdentity.Impersonate();
                            //save request in DB
                            SaveRequestInDB(sender, e);
                        }
                    }
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Analytics Impersonate -- context_PostRequestHandlerExecute", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                }
                finally
                {
                    if (impersonatedContext != null) { impersonatedContext.Undo(); }
                }
            });
        }

        /// <summary>
        /// Save request in analytics database
        /// </summary>
        /// <param name="sender">get HttpApplication object</param>
        /// <param name="e">get event args</param>
        private void SaveRequestInDB(object sender, EventArgs e)
        {
            context = ((HttpApplication)sender).Context;
            string userName = context.User.Identity.Name;
            bool isValidUser = IsValidUser(userName);
            if (isValidUser)
            {
                string rawURL = context.Request.RawUrl.ToLower();
                string OrgURL = context.Request.Url.ToString().ToLower();


                if (this.ValidateURL(OrgURL))
                {
                    string url = this.ProcessURL(rawURL).ToLower();
                    if (!string.IsNullOrEmpty(url))
                    {
                        bool pageAllowed = IsPageAllowed(url);
                        if (pageAllowed)
                        {
                            url = HttpContext.Current.Server.UrlDecode(url);
                            OrgURL = HttpContext.Current.Server.UrlDecode(OrgURL);
                            //save request in DB
                            SaveRequestInDB(userName, url, OrgURL, GetUserIP(context), context.Request.Browser.Browser);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Save accessed URL along with other properties into SQL table
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="URL"></param>
        /// <param name="OrgURL"></param>
        /// <param name="UserIP"></param>
        /// <param name="Browser"></param>
        private void SaveRequestInDB(string userName, string URL, string OrgURL, string UserIP, string Browser)
        {
            string uspLofAccessedURL = Convert.ToString(ConfigurationManager.AppSettings[HttpModuleConstants.SpLogAccessedURL]);
            string conString = Convert.ToString(ConfigurationManager.ConnectionStrings[Constants.AnalyticsDBConstants.ConnectionStringForAnalyticsDB_Windows]);
            if (!string.IsNullOrEmpty(uspLofAccessedURL) && !string.IsNullOrEmpty(conString))
            {
                sqlConnection = new SqlConnection(conString);
                sqlConnection.Open();
                SqlCommand command = new SqlCommand(uspLofAccessedURL.Trim(), sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                command.Parameters.Add("@URL", SqlDbType.VarChar).Value = URL;
                //TG: Change date time to be stored in UTC
                command.Parameters.Add("@Timestamp", SqlDbType.DateTime).Value = DateTime.Now;
                command.Parameters.Add("@UserIP", SqlDbType.VarChar).Value = UserIP;
                command.Parameters.Add("@Browser", SqlDbType.VarChar).Value = Browser;
                if (URL.EndsWith(".aspx"))
                {
                    command.Parameters.Add("@Type", SqlDbType.VarChar).Value = HttpModuleConstants.TypeAPSX;
                    command.Parameters.Add("@DocTitle", SqlDbType.VarChar).Value = HttpUtility.UrlDecode(URL.Substring(URL.LastIndexOf('/') + 1).Replace(".aspx", string.Empty));
                }
                else
                {
                    command.Parameters.Add("@Type", SqlDbType.VarChar).Value = HttpModuleConstants.TypeDocument;
                    command.Parameters.Add("@DocTitle", SqlDbType.VarChar).Value = HttpUtility.UrlDecode(URL.Substring(URL.LastIndexOf('/') + 1).Replace("." + URL.Substring(URL.LastIndexOf('.') + 1), string.Empty));
                }
                command.Parameters.Add("@OriginalURL", SqlDbType.VarChar).Value = OrgURL;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Skip requests except .aspx & office files
        /// </summary>
        /// <param name="URL">Requested URL</param>
        /// <returns>Formated URL</returns>
        private bool ValidateURL(string url)
        {
            bool isValid = true;
            string filesTypeNotAllowed = Convert.ToString(ConfigurationManager.AppSettings[HttpModuleConstants.FilesTypeNotAllowed]);
            if (url.Contains("jquery"))
            {
                isValid = false;
            }
            else
            {
                string[] arrFilesNotAllowed = filesTypeNotAllowed.Split(';');
                if (arrFilesNotAllowed != null && arrFilesNotAllowed.Length > 0)
                {
                    for (int iCount = 0; iCount < arrFilesNotAllowed.Length; iCount++)
                    {
                        if (url.Contains("." + arrFilesNotAllowed[iCount]))
                        {
                            isValid = false;
                            break;
                        }
                    }

                }
            }
            return isValid;
        }

        /// <summary>
        /// Get valid url from request
        /// </summary>
        /// <param name="URL">Web requested URL</param>
        /// <returns>formated URL</returns>
        private string ProcessURL(string url)
        {
            string validURL = string.Empty;
            StringBuilder sb = new StringBuilder();
            string[] arrSplitByQs = url.Split('?');
            if (url.Contains("wopiframe"))
            {
                string[] arrSplitAmp = arrSplitByQs[1].Split('&');

                if (arrSplitAmp != null && arrSplitAmp.Length > 0)
                {
                    for (int i = 0; i < arrSplitAmp.Length; i++)
                    {
                        string[] arrSplitEq = arrSplitAmp[i].Split('=');
                        if (arrSplitEq[0].Equals("sourcedoc", StringComparison.OrdinalIgnoreCase))
                        {
                            string modifyURL = SPContext.Current.Web.Site.WebApplication.GetResponseUri(Microsoft.SharePoint.Administration.SPUrlZone.Default).AbsoluteUri;
                            if (modifyURL.EndsWith("/"))
                            {
                                modifyURL = modifyURL.Substring(0, modifyURL.LastIndexOf('/'));
                            }
                            validURL = modifyURL + arrSplitEq[1];

                        }
                    }
                }
            }
            else if (arrSplitByQs[0].EndsWith(".aspx"))
            {
                var uri = new UriBuilder(HttpContext.Current.Request.Url);
                uri.Path = HttpContext.Current.Request.RawUrl;
                validURL = HttpContext.Current.Server.UrlDecode(uri.Uri.AbsoluteUri).Split(new[] { '?' })[0];

            }
            else
            {
                // get url of MS office files
                string filesTypeAllowed = Convert.ToString(ConfigurationManager.AppSettings[HttpModuleConstants.FilesTypeAllowed]);
                string[] arrAllowFiles = filesTypeAllowed.Split(';');
                if (arrAllowFiles != null && arrAllowFiles.Length > 0)
                {
                    string tempURL = context.Request.Url.GetLeftPart(UriPartial.Path);
                    string reqFileExt = tempURL.Substring(tempURL.LastIndexOf('.') + 1);
                    int fileExistCount = Array.IndexOf(arrAllowFiles, reqFileExt);
                    if (fileExistCount > -1)
                    {
                        var uri = new UriBuilder(HttpContext.Current.Request.Url);
                        uri.Path = HttpContext.Current.Request.RawUrl;
                        validURL = HttpContext.Current.Server.UrlDecode(uri.Uri.AbsoluteUri).Split(new[] { '?' })[0];
                    }
                }
            }
            return validURL;
        }

        /// <summary>
        /// Validate logged-in user.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        private bool IsValidUser(string userName)
        {
            bool isValidUser = true;
            string excludeUserAccounts = Convert.ToString(ConfigurationManager.AppSettings[HttpModuleConstants.ExcludeUserAccounts]);
            string[] arrExcludeUserAccounts = excludeUserAccounts.Split(';');
            if (arrExcludeUserAccounts != null && arrExcludeUserAccounts.Length > 0)
            {
                int userExistCount = Array.IndexOf(arrExcludeUserAccounts, userName);
                if (userExistCount > -1)
                {
                    isValidUser = false;
                }
            }
            return isValidUser;
        }

        /// <summary>
        /// Check if page allowed or not
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private bool IsPageAllowed(string url)
        {
            bool pageAllowed = true;
            string pagesTypeNotAllowed = Convert.ToString(ConfigurationManager.AppSettings[HttpModuleConstants.ExcludePages]);
            if (!String.IsNullOrEmpty(pagesTypeNotAllowed))
            {
                string[] arrNotAllowedPages = pagesTypeNotAllowed.ToLower().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (arrNotAllowedPages != null && arrNotAllowedPages.Length > 0)
                {
                    var pageNotAllowed = arrNotAllowedPages.FirstOrDefault(c => url.ToLower().Contains(c.ToLower()));
                    if (pageNotAllowed != null)
                    {
                        pageAllowed = false;
                    }
                }
            }
            return pageAllowed;
        }

        /// <summary>
        /// Validate site collection before saving data
        /// </summary>
        /// <param name="siteCollectionUrl"></param>
        /// <returns></returns>
        private bool ValidSiteCollection(string siteCollectionUrl)
        {
            bool isValidSiteCollection = true;
            string allowedSiteCollections = Convert.ToString(ConfigurationManager.AppSettings[HttpModuleConstants.AllowedSiteCollection]);
            if (!String.IsNullOrEmpty(allowedSiteCollections))
            {
                string[] arrAllowedSiteCollections = allowedSiteCollections.ToLower().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (arrAllowedSiteCollections != null && arrAllowedSiteCollections.Length > 0)
                {
                    var siteCollectionExists = arrAllowedSiteCollections.FirstOrDefault(c => siteCollectionUrl.Equals(c.ToLower(), StringComparison.OrdinalIgnoreCase));
                    if (siteCollectionExists == null)
                    {
                        isValidSiteCollection = false;
                    }
                }
            }
            return isValidSiteCollection;
        }

        public string GetUserIP(HttpContext context)
        {
            var ip = (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
            && context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
            ? context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
            : context.Request.ServerVariables["REMOTE_ADDR"];
            if (ip.Contains(","))
                ip = ip.Split(',').First().Trim();
            return ip;
        }
    }

    /// <summary>
    /// Constants of HTTP modules
    /// </summary>
    public static class HttpModuleConstants
    {
        public const string AllowedSiteCollection = "AdSpaceAnalyticsAllowSiteCollection";
        public const string FilesTypeAllowed = "AdSpaceAnalyticsAllowFileExtension";
        public const string FilesTypeNotAllowed = "AdSpaceAnalyticsDoNotAllowFileExtension";
        public const string ExcludeUserAccounts = "AdSpaceAnalyticsExcludeUserAccounts";
        public const string ExcludePages = "AdSpaceAnalyticsExcludePages";
        public const string SpLogAccessedURL = "SpLogAccessedURL";
        public const string RESTRICTED_MODULES = "add[@name='AnalyticsHttpModule']";
        public const string MODULE_PATH = "configuration/system.webServer/modules";
        public const string ASSEMBLY_PATH = @"<add name=""AnalyticsHttpModule"" type=""Viacom.Adspace.AnalyticsHttpModule.CaptureWebRequest, {0}"" />";
        public const string TypeAPSX = "ASPX";
        public const string TypeDocument = "Document";
        public const string EnvelopeSTag = "<s:Envelope";
        public const string EnvelopeETag = "</s:Envelope>";
        public const string CellStorageService = "cellstorageservice";
        public const string RequestXMLNode = "Request";
        public const string UrlXMLAttribute = "Url";
        public const string Browser = "IE - Office Client Application";
        public const string UnknownBrowser = "Unknown";
    }
}
